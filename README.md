## **DATA ENGINEERING TEST**

---

#### **- Test_sql.docx File**

SQL Test Solution

#### **- prueba.py File**

Challenge solution "CAPTCHA me if you can" in python, with the help of the selenium library to control the web browser through a program, thus logging in to the Root me page, to access the captcha generated in the challenge, where a screenshot of this is captured and through opencv a processing of the image is done to crop it and obtain only the captchat box, in addition to applying filters to leave it in gray scale to eliminate noise, once this image is obtained we proceed to extract the text of the image with the easyocr library, finally send this text in the captcha input and validate it.

For the execution of the project it will be necessary to install the libraries numpy, selenium, opencv, easyocr, imutils. Likewise, it will be necessary to pass the parameters of mail, password and location of the "chromedriver.exe" file. Once you have all these tools to run the project, all you have to do is run the test.py file.

### To run the project perform the following steps

1.  clone the project

    	git clone https://gitlab.com/sofiat9909/prueba-para-ingeniera-de-datos.git
2.  locate ourselves in the project

    	cd .\prueba-para-ingeniera-de-datos\
3.  Download libraries required for the project

    	pip install -r .\requirements.txt
4.  run the project

    	python prueba.py
