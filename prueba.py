from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from pylab import rcParams
from IPython.display import Image
from PIL import Image

import time
import os
import matplotlib.pyplot as plt
import cv2
import easyocr
import imutils


class CaptchaBreak:
    URL = "https://www.root-me.org/?lang=es"
    URL_CHALLENGE = "http://challenge01.root-me.org/programmation/ch8/"

    def __init__(self, username: str, password: str, path_chrome: str = None):
        self.username = username
        self.password = password
        self.path_chrome = path_chrome

    """ The get_username, get_password and get_chrome methods are implemented to 
        prompt users for the data needed to initialize the code."""

    def get_username() -> str:
        username = input("Enter your username. ")
        return username

    def get_password() -> str:
        password = input("Enter your password. ")
        return password

    def get_chrome() -> str:
        chrome = input("Enter your rout for chromedriver.exe. ")
        return chrome

    def setup(self):
        """The connection to the chrome driver is made and the root me URL is entered"""
        self.driver = webdriver.Chrome(self.path_chrome)
        self.driver.get(self.URL)
        self.driver.implicitly_wait(40)

    def login(self) -> None:
        """Login to the page"""
        self.driver.find_element(By.XPATH, '//a[@title="cierrar"]').click()
        self.driver.find_element(By.XPATH, '//a[@title="Ya tengo una cuenta"]').click()
        self.driver.find_element(By.ID, "var_login").send_keys(self.username)
        self.driver.find_element(By.ID, "password").send_keys(self.password)

        element = self.driver.find_elements(By.XPATH, '//input[@class = "btn submit"]')
        element[1].click()

    def start_challenge(self) -> None:
        """The page where the captcha of the challenge is obtained is entered and a
        screenshot is taken"""
        self.driver.get(self.URL_CHALLENGE)
        self.driver.get_screenshot_as_file("image_captcha.png")

    def procesing_image(self) -> None:
        """A screenshot is processed, resizing the image and converting it to gray scale"""
        image = cv2.imread("image_captcha.png")
        image_resize = image[90:150, 20:400]
        cv2.imwrite("resize.png", image_resize)
        image = cv2.imread("resize.png")
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        se = cv2.getStructuringElement(cv2.MORPH_RECT, (8, 8))
        bg = cv2.morphologyEx(image, cv2.MORPH_DILATE, se)
        image_gray = cv2.divide(image, bg, scale=255)
        image_binary = cv2.threshold(image_gray, 0, 255, cv2.THRESH_OTSU)[1]
        cv2.imwrite("binary.png", image_binary)

    def extract_text_image(self) -> None:
        """Extract the text from the image and get it as a string"""
        rcParams["figure.figsize"] = 6, 18
        reader = easyocr.Reader(["en"])
        self.text = reader.readtext("binary.png")[0][1]

    def pass_captcha(self) -> None:
        print(self.text)
        """The captcha obtained by the program is sent"""
        self.driver.find_element(By.XPATH, '//input[@name = "cametu"]').send_keys(
            self.text[:-1]
        )
        self.driver.find_element(By.XPATH, '//input[@value = "Try"]').click()

    def tearDown(self):
        os.remove("image_captcha.png")
        os.remove("resize.png")
        os.remove("binary.png")
        self.driver.quit()
        time.sleep(60)


def main():
    username = CaptchaBreak.get_username()
    password = CaptchaBreak.get_password()
    chrome = CaptchaBreak.get_chrome()
    captcha = CaptchaBreak(
        username,
        password,
        chrome,
    )
    return captcha


if __name__ == "__main__":
    captcha_bot = main()
    captcha_bot.setup()
    captcha_bot.login()
    captcha_bot.start_challenge()
    captcha_bot.procesing_image()
    captcha_bot.extract_text_image()
    captcha_bot.pass_captcha()
    captcha_bot.tearDown()
